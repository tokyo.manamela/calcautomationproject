package reports;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class reports {
	
	public ExtentReports report2(String reports){
		 
		 ExtentHtmlReporter HtmlReporter  = new ExtentHtmlReporter(reports);
		 ExtentReports extent = new ExtentReports();
		 HtmlReporter.config().setCSS(".r-img{width:60%}");
		 
		 HtmlReporter.config().setTheme(Theme.DARK);
		 HtmlReporter.setAppendExisting(true); 
		 extent.attachReporter(HtmlReporter);
		 
		 return extent;
	  }
	  
	  public ExtentTest createTest(ExtentReports extent, String name) {
		  
		  ExtentTest test = extent.createTest(name);
		  return test;
		   
	  }
	  
	  
	 
	  
	  
	  
	  
	  public static String ScrShotPath(WebDriver driver)
		{
			TakesScreenshot ts=(TakesScreenshot) driver;
			
			File src=ts.getScreenshotAs(OutputType.FILE);
			
			String path="./Screenshots/"+System.currentTimeMillis()+".png";
			
			File destination=new File(path);
			
			try 
			{
				FileUtils.copyFile(src, destination);
			} catch (IOException e) 
			{
				System.out.println("Capture Failed "+e.getMessage());
			}
			
			return path;
		}
	
	

}
