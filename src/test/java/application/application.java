package application;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import Utilities.UtilitiesApp;
import reports.reports;


public class application {
	
	private Recordset recordset;
	ResultSet res = null;
	UtilitiesApp Util_obj = new UtilitiesApp(); 
	static reports rep_obj = new reports();
    public static ExtentReports rep= null;
	
	
	
	
	

	
	
	public void testPlusButton(String expected , String TestName, String description) throws SQLException, IOException {
		
		res.next();
		retrievData(expected, TestName, description);
	}
	
	
	
public void testScientificMode(String expected , String TestName, String description) throws SQLException, IOException {
	
	res.next();
	retrievData(expected, TestName, description);
	  
		
	}

public void testPowerButton(String expected , String TestName, String description) throws SQLException, IOException {
	
	res.next();
	retrievData(expected, TestName, description);
	  	
	}

public void testingProgrammerMode(String expected , String TestName, String description) throws SQLException, IOException {
	
	res.next();
	retrievData(expected, TestName, description);
	
}


public void Createdatabase() throws SQLException {
	
	
	
	 Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");

     Statement stt = con.createStatement();

     stt.execute("CREATE DATABASE IF NOT EXISTS TokyoCalcAuto");
     stt.execute("USE TokyoCalcAuto");

     stt.execute("DROP TABLE IF EXISTS inputs");
     stt.execute("CREATE TABLE inputs  (" +
             "id BIGINT NOT NULL AUTO_INCREMENT,"
             + "firstNum varchar(25) NOT NULL,"
             + "Sign varchar(25) NOT NULL,"
             + "secondNum varchar(25) NOT NULL,"
             + "Result varchar(25) NOT NULL,"
             + "Mode_change varchar(25) NOT NULL,"
             + "Mode varchar(25) NOT NULL,"
             + "equals varchar(25) NOT NULL,"
             +"PRIMARY KEY(id)"
             + ")");
     stt.execute("INSERT INTO `inputs` (`firstNum`, `Sign`, `secondNum`, `Result`, `Mode_change`, `Mode`, `equals`) VALUES"
 		 +" ('num1Button', 'plusButton', 'num7Button', 'CalculatorResults', 'TogglePaneButton', 'Standard', 'equalButton'),"
  		 + "('num9Button', 'powerButton', 'num3Button', 'CalculatorResults', 'TogglePaneButton', 'Scientific', 'equalButton')," 
  		 +"('num9Button',  'minusButton','num2Button', 'binaryButton', 'TogglePaneButton', 'Programmer', 'equalButton'),"
  		 +"('num7Button',  'modButton','num2Button', 'CalculatorResults', 'TogglePaneButton', 'Scientific', 'equalButton')");
     
     res = stt.executeQuery("SELECT * FROM inputs ");
}


public void retrievData(String Expected, String TestName, String description) throws SQLException, IOException {
	
	

  	 
	
  	Util_obj.driver.findElementByAccessibilityId(res.getString("Mode_change")).click();
	Util_obj.driver.findElementByAccessibilityId(res.getString("Mode")).click();
	Util_obj.driver.findElementByAccessibilityId(res.getString("firstNum")).click();
	Util_obj.driver.findElementByAccessibilityId(res.getString("Sign")).click();
	Util_obj.driver.findElementByAccessibilityId(res.getString("secondNum")).click();
	Util_obj.driver.findElementByAccessibilityId(res.getString("equals")).click();
	
	
	String ActualResult = Util_obj.driver.findElementByAccessibilityId(res.getString("Result")).getText();
  
  	if(ActualResult.equals(Expected)) {
  		 
  		
  		 rep = rep_obj.report2("reports\\rep.html");
		  ExtentTest test = rep_obj.createTest(rep, TestName);
 	      test.assignAuthor("Manamela Tokyo");
		  test.log(Status.PASS,description );
		  String path = reports.ScrShotPath(Util_obj.driver);
		  test.pass("Screenshot", MediaEntityBuilder.createScreenCaptureFromPath("."+path).build());
		  rep.flush();
  	}
  	else {
  		

  	
  		rep = rep_obj.report2("reports\\rep.html");
		  ExtentTest test = rep_obj.createTest(rep, TestName);
	      test.assignAuthor("Manamela Tokyo");
		  test.log(Status.FAIL,description );
		  String path = reports.ScrShotPath(Util_obj.driver);
		  test.pass("Screenshot", MediaEntityBuilder.createScreenCaptureFromPath("."+path).build());
		  rep.flush();
  	}
  
  	
  	
  	
}




}
