package Utilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.windows.WindowsDriver;
import junit.framework.Assert;

public class UtilitiesApp {
	
	
	 public static WindowsDriver driver = null;
	
	  public void setup() throws MalformedURLException {
	      
          DesiredCapabilities capabilities = new DesiredCapabilities();
          capabilities.setCapability("app", "Microsoft.WindowsCalculator_8wekyb3d8bbwe!App");
          driver = new WindowsDriver(new URL("http://127.0.0.1:4723"), capabilities);
          driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
          
  }
	  public void quit() {
		  
		  driver.quit();
		  
	  }
	  


}
