package RunApp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.codoid.products.exception.FilloException;

import Utilities.UtilitiesApp;
import application.application;
import io.appium.java_client.windows.WindowsDriver;
import junit.framework.Assert;
import reports.reports;

public class test {
	
	
	UtilitiesApp Util_obj = new UtilitiesApp(); 
	application app_obj = new application();
	static reports rep_obj = new reports();
	public static ExtentReports rep= null;
	
	
    @BeforeClass
    public void setUp() throws MalformedURLException {
    	
    	Util_obj.setup();
    	
    }
  
    
	
  @Test
  public void f() throws FilloException, IOException, SQLException {
	  
	  
	
	  app_obj.Createdatabase();

	//app_obj.testDevideButton("Display is Cannot divide ", "Division sign test", "We test if the division sign is working, refer to screenshot below");
	  
	  app_obj.testPlusButton("Display is 8", "Plus sign test", "We test if the plus sign is working, by adding two numbers. refer to screenshot below");
	  app_obj.testScientificMode("Display is 729", "Scientific sign test", "We test if the scientific Mode is working, by calculating 9 ^10, refer to screenshot below");
	  app_obj.testingProgrammerMode("Binary 0 1 1 1 ", "Programmer mode test", "we test if the programmer mode is working, by calculating the binary value 9-2(7)");
	  app_obj.testPowerButton("Display is 1", "Modulas button test", "we are testing the Modulas button and  the test data is 7Mod2 which should give us 1 refer to the screenshot below");
	  
	  
	  } 
  
  @AfterTest
  public void PostTest() {
	  
	  Util_obj.quit();
	  
	  
  }
 

}
